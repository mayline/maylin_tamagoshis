package org.example.Tamagoshi;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NormalTamagoshiTest {

    private NormalTamagoshi tamagotchi;

    @BeforeEach
    void setUp() {
        tamagotchi = new NormalTamagoshi("Tommy");
    }

    @Test
    void testInitialState() {
        // Vérifier l'état initial du Tamagotchi
        assertEquals("Tommy", tamagotchi.getName());
        assertTrue(tamagotchi.getcountEnergie() >= 3 && tamagotchi.getcountEnergie() <= 5, "L'énergie initiale doit être entre 3 et 5");
        assertTrue(tamagotchi.getMaxEnergie() >= 5 && tamagotchi.getMaxEnergie() <= 9, "L'énergie maximale doit être entre 5 et 9");
        assertTrue(tamagotchi.getLevelFoodAlert() >= 3 && tamagotchi.getLevelFoodAlert() <= 5, "Le niveau d'alerte de nourriture doit être entre 3 et 5");
        assertTrue(tamagotchi.getCountFood() >= 3 && tamagotchi.getCountFood() <= 5, "La quantité initiale de nourriture doit être entre 3 et 5");
        assertTrue(tamagotchi.isAlive(), "Le Tamagotchi devrait être vivant au départ");
    }

    @Test
    void testFun() {
        // Vérifier le comportement de la méthode fun()
        tamagotchi.setCountEnergie(3);
        int initialEnergie = tamagotchi.getcountEnergie();

        // Appel de la méthode fun(), cela doit augmenter l'énergie.
        tamagotchi.fun();

        // Vérifier que l'énergie a augmenté (ne dépasse pas l'énergie maximale).
        assertTrue(tamagotchi.getcountEnergie() > initialEnergie, "L'énergie devrait augmenter après l'interaction FUN");
        assertTrue(tamagotchi.getcountEnergie() <= tamagotchi.getMaxEnergie(), "L'énergie ne doit pas dépasser la valeur maximale après l'interaction FUN");
    }

    @Test
    void testFeed() {
        // Vérifier le comportement de la méthode feed()
        int initialFood = tamagotchi.getCountFood();

        // Appel de la méthode feed(), cela doit augmenter la nourriture.
        tamagotchi.feed();

        // Vérifier que la nourriture a augmenté (ne dépasse pas la nourriture maximale).
        assertTrue(tamagotchi.getCountFood() > initialFood, "La nourriture devrait augmenter après l'interaction FEED");
    }

    @Test
    void testStateWhenHungry() {
        // Si la nourriture est faible, le Tamagotchi devrait indiquer qu'il a faim
        tamagotchi = new NormalTamagoshi("Tommy");
        tamagotchi.setCountFood(2);
        tamagotchi.setCountEnergie(5);

        assertTrue(tamagotchi.state().contains("a faim"), "Le message d'état devrait indiquer que le Tamagotchi a faim lorsque la nourriture est faible");
    }

    @Test
    void testStateWhenBored() {
        // Si l'énergie est faible, le Tamagotchi devrait indiquer qu'il s'ennuie
        tamagotchi = new NormalTamagoshi("Tommy");
        tamagotchi.setCountFood(5);
        tamagotchi.setCountEnergie(2);

        assertTrue(tamagotchi.state().contains("s'ennuie"), "Le message d'état devrait indiquer que le Tamagotchi s'ennuie lorsque l'énergie est faible");
    }

    @Test
    void testStateWhenHealthy() {
        // Quand tout va bien, le Tamagotchi devrait afficher "Tout va bien !!!"
        tamagotchi = new NormalTamagoshi("Tommy");

        // Réinitialiser les niveaux d'énergie et de nourriture
        tamagotchi.setCountEnergie(6);
        tamagotchi.setCountFood(6);

        assertTrue(tamagotchi.state().contains("Tout va bien !!!"), "Le message d'état devrait indiquer que tout va bien lorsque l'énergie et la nourriture sont suffisants");
    }

    @Test
    void testNextDaySurvive() {
        // Vérifier que le Tamagotchi survit un jour sans mourir
        tamagotchi.nextDay();
        assertTrue(tamagotchi.isAlive(), "Le Tamagotchi devrait être vivant après un jour");
    }

    @Test
    void testNextDayDeathFromHunger() {
        // Vérifier que le Tamagotchi meurt de faim
        tamagotchi = new NormalTamagoshi("Tommy");

        tamagotchi.setCountFood(5);
        tamagotchi.setCountEnergie(5);
        tamagotchi.nextDay();
        assertTrue(tamagotchi.isAlive(), "Le Tamagotchi devrait être vivant");


        tamagotchi.setCountFood(0);
        tamagotchi.nextDay();
        assertFalse(tamagotchi.isAlive(), "Le Tamagotchi devrait être mort après plusieurs jours sans nourriture");
    }

    @Test
    void testNextDayDeathFromFatigue() {
        // Vérifier que le Tamagotchi meurt de fatigue
        tamagotchi = new NormalTamagoshi("Tommy");

        tamagotchi.setCountFood(5);
        tamagotchi.setCountEnergie(5);
        tamagotchi.nextDay();
        assertTrue(tamagotchi.isAlive(), "Le Tamagotchi devrait être vivant");

        tamagotchi.setCountEnergie(0);
        tamagotchi.nextDay();
        assertFalse(tamagotchi.isAlive(), "Le Tamagotchi devrait être mort après plusieurs jours sans énergie");
    }
}
