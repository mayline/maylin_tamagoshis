package org.example.Tamagoshi;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AbstractTamagoshiTest {

    private NormalTamagoshi tamagotchi;

    @BeforeEach
    void setUp() {
        tamagotchi = new NormalTamagoshi("Tommy");
    }

    @Test
    void testInitialState() {
        // Vérifier l'état initial du Tamagotchi
        assertEquals("Tommy", tamagotchi.getName());
        assertTrue(tamagotchi.getcountEnergie() >= 3 && tamagotchi.getcountEnergie() <= 5, "L'énergie initiale doit être entre 3 et 5");
        assertTrue(tamagotchi.getMaxEnergie() >= 5 && tamagotchi.getMaxEnergie() <= 9, "L'énergie maximale doit être entre 5 et 9");
        assertTrue(tamagotchi.getLevelFoodAlert() >= 3 && tamagotchi.getLevelFoodAlert() <= 5, "Le niveau d'alerte de nourriture doit être entre 3 et 5");
        assertTrue(tamagotchi.getCountFood() >= 3 && tamagotchi.getCountFood() <= 5, "La quantité initiale de nourriture doit être entre 3 et 5");
        assertTrue(tamagotchi.isAlive(), "Le Tamagotchi devrait être vivant au départ");
    }

    @Test
    void testState() {
        // Vérifier les états possibles du Tamagotchi
        tamagotchi = new NormalTamagoshi("Tommy");

        // Quand l'énergie est faible
        tamagotchi.setCountEnergie(2);
        tamagotchi.setCountFood(5);
        assertTrue(tamagotchi.state().contains("s'ennuie"), "Le message d'état devrait indiquer que le Tamagotchi s'ennuie quand l'énergie est faible");

        // Quand la nourriture est faible
        tamagotchi.setCountEnergie(5);
        tamagotchi.setCountFood(2);
        assertTrue(tamagotchi.state().contains("a faim"), "Le message d'état devrait indiquer que le Tamagotchi a faim quand la nourriture est faible");

        // Quand tout va bien
        tamagotchi.setCountEnergie(5);
        tamagotchi.setCountFood(5);
        assertTrue(tamagotchi.state().contains("Tout va bien !!!"), "Le message d'état devrait indiquer que tout va bien lorsque l'énergie et la nourriture sont suffisantes");
    }

    @Test
    void testNextDay() {
        tamagotchi.setAge(2);
        tamagotchi.setCountFood(2);
        tamagotchi.setCountEnergie(2);
        int initialFood = tamagotchi.getCountFood();
        int initialEnergie = tamagotchi.getcountEnergie();
        int initialAge = tamagotchi.age;

        // Avancer d'un jour
        tamagotchi.nextDay();

        assertEquals(initialFood - 1, tamagotchi.getCountFood(), "La nourriture devrait diminuer de 1 après un jour");
        assertEquals(initialEnergie - 1, tamagotchi.getcountEnergie(), "L'énergie devrait diminuer de 1 après un jour");
        assertEquals(initialAge + 1, tamagotchi.age, "L'âge devrait augmenter de 1 après un jour");
    }

    @Test
    void testDeadTamagotchi() {
        // Assurez-vous que le Tamagotchi meurt de faim
        tamagotchi = new NormalTamagoshi("Tommy");
        tamagotchi.nextDay();
        tamagotchi.nextDay(); // On réduit encore la nourriture

        assertTrue(tamagotchi.isAlive(), "Le Tamagotchi devrait être vivant au départ");

        // Faire en sorte que le Tamagotchi meure de faim
        tamagotchi.nextDay();
        tamagotchi.nextDay();
        tamagotchi.nextDay();
        tamagotchi.nextDay();
        assertFalse(tamagotchi.isAlive(), "Le Tamagotchi devrait être mort après plusieurs jours sans nourriture");
    }

    @Test
    void testInteragirFun() {
        int initialEnergie = tamagotchi.getcountEnergie();
        tamagotchi.interagir(Interaction.FUN);
        assertTrue(tamagotchi.getcountEnergie() > initialEnergie, "L'énergie du Tamagotchi devrait augmenter après l'interaction FUN");
    }

    @Test
    void testInteragirFeed() {
        int initialFood = tamagotchi.getCountFood();
        tamagotchi.interagir(Interaction.FEED);
        assertTrue(tamagotchi.getCountFood() > initialFood, "La nourriture du Tamagotchi devrait augmenter après l'interaction FEED");
    }
}
