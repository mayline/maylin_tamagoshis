package org.example.Tamagoshi;

public class NormalTamagoshi extends AbstractTamagoshi{

    public NormalTamagoshi(String name) {
        super(name);
        this.count_food = random.nextInt(3,5);
        this.max_food = random.nextInt(3,5);
        this.level_food_alert = random.nextInt(3,5);
    }

    @Override
    protected void fun() {
        this.count_energie = Math.max(this.count_energie+random.nextInt(1,3), this.max_energie);
    }

    @Override
    protected void feed() {
        this.count_food = Math.max(this.count_food+random.nextInt(1,3), this.max_food);
    }


}
