package org.example.Tamagoshi;

import java.util.Random;

public abstract class AbstractTamagoshi {
    protected static final Random random = new Random();
    protected static final int AGE_LIMIT = 10;

    protected final String name;
    protected int age;
    protected int count_energie = random.nextInt(3,5);
    protected final int max_energie = random.nextInt(5, 9);
    protected final int level_energie_alerte = random.nextInt(3, 5);
    protected int count_food;
    protected int max_food;
    protected int level_food_alert;
    protected boolean dead = false;

    public AbstractTamagoshi(String name) {
        this.name = name;
    }

    protected abstract void fun();

    protected abstract void feed();

    public String getName() {
        return name;
    }

    public String state() {
        StringBuilder message = new StringBuilder();
        if(count_energie <= level_energie_alerte) {
            message.append(this).append(" s'ennuie... \n");
        }
        else if(count_food <= level_food_alert) {
            message.append(this).append(" a faim... \n");
        }
        else {
            message.append(this).append("Tout va bien !!!");
        }
        return message.toString();
    }

    @Override
    public String toString() {
        return "AbstractTamagoshi{" +
                "name='" + name + '\'' +
                ", count_energie=" + count_energie +
                ", count_food=" + count_food +
                ", dead=" + dead +
                '}';
    }

    public void interagir(Interaction interaction) {
        String messsage = "";
        switch (interaction) {
           case FUN -> this.fun();
           case FEED -> this.feed();
        }

    };

    public void nextDay() {
        if (this.count_food <= 0 ) {
            this.dead = true;
            System.out.println(this+" est mort de faim...");
            return;
        }
        if ( this.count_energie <= 0) {
            this.dead = true;
            System.out.println(this+" est mort de fatigue...");
            return;
        }
        this.count_food-=1;
        this.count_energie-=1;
        this.age+=1;
    }

    public boolean isAlive() {
        return !this.dead;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getcountEnergie() {
        return count_energie;
    }

    public void setCountEnergie(int count_energie) {
        this.count_energie = count_energie;
    }

    public int getMaxEnergie() {
        return max_energie;
    }

    public int getLevelEnergieAlerte() {
        return level_energie_alerte;
    }

    public int getCountFood() {
        return count_food;
    }

    public void setCountFood(int count_food) {
        this.count_food = count_food;
    }

    public int getMax_food() {
        return max_food;
    }

    public void setMaxFood(int max_food) {
        this.max_food = max_food;
    }

    public int getLevelFoodAlert() {
        return level_food_alert;
    }

    public void setLevelFoodAlert(int level_food_alert) {
        this.level_food_alert = level_food_alert;
    }

    public boolean isDead() {
        return dead;
    }

    public void setDead(boolean dead) {
        this.dead = dead;
    }
}
