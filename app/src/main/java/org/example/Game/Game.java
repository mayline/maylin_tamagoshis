package org.example.Game;

import org.example.Tamagoshi.Interaction;
import org.example.Tamagoshi.NormalTamagoshi;

import java.util.Scanner;

public class Game {
    private NormalTamagoshi[] tamagoshis;
    private int nbTamagoshisInitiale;
    private Scanner scanner;

    public Game(Scanner scanner) {
        this.scanner = scanner;
        this.initGame();

        for (int i = 1; i <= 10; i++) {
            System.out.println("\n------------ Tour n°"+ i +" -------------\n");
            for (NormalTamagoshi tamagoshi:
                 this.tamagoshis) {
                if(tamagoshi.isAlive()) {
                    System.out.println(tamagoshi.state());
                }
            }

            System.out.println("Nourir quel tamagoshi ?");
            for (int j = 1; j <= this.tamagoshis.length; j++) {
                if(this.tamagoshis[j - 1].isAlive()){
                    System.out.print("("+j+") "+this.tamagoshis[j-1]+"  ");
                }
            }
            System.out.println("\nEntrez un choix : ");
            int choixFeed = this.scanner.nextInt() - 1;
            this.tamagoshis[choixFeed].interagir(Interaction.FEED);


            System.out.println("Jouer avec quel tamagoshi ?");
            for (int j = 1; j <= this.tamagoshis.length; j++) {
                if(this.tamagoshis[j - 1].isAlive()){
                    System.out.print("("+j+") "+this.tamagoshis[j-1]+"  ");
                }
            }
            System.out.println("\nEntrez un choix : ");
            int choixFun = this.scanner.nextInt() - 1;
            this.tamagoshis[choixFun].interagir(Interaction.FUN);

            for (NormalTamagoshi tamagoshi:
                    this.tamagoshis) {
                if(tamagoshi.isAlive()){
                    tamagoshi.nextDay();
                }
            }
        }




    }

    private void initGame() {
        System.out.println("Entrez le nombre de tamagoshis désiré !");
        this.nbTamagoshisInitiale = this.scanner.nextInt();
        this.tamagoshis = new NormalTamagoshi[nbTamagoshisInitiale];
        for (int i = 1; i <= nbTamagoshisInitiale; i++) {
            System.out.println("Entrez le nom du tamagoshi numéro "+ i +" :");
            String nomTamagoshi = this.scanner.next();
            tamagoshis[i-1] = new NormalTamagoshi(nomTamagoshi);
        }
    }

    public NormalTamagoshi[] getTamagoshis() {
        return tamagoshis;
    }


}
